﻿#pragma once

#include <sstream>

#include "ListNode.h"
#include "ListRand.h"
#include "Utils.h"

class Tester {
public:
    static bool EqualityTest() {
        ListRand List1(5);
        ListRand List2(5);
        Utils::GenerateSequenceData(List1);
        Utils::GenerateSequenceData(List2);
        return List1 == List2;
    }

    static bool NonEqualityTest() {
        ListRand List1(5);
        ListRand List2(5);
        Utils::GenerateSequenceData(List1);
        Utils::GenerateSequenceData(List2, 2);
        return !(List1 == List2);
    }

    static bool DifferentLengthTest() {
        ListRand List1(5);
        ListRand List2(2);
        Utils::GenerateSequenceData(List1);
        Utils::GenerateSequenceData(List2);
        return !(List1 == List2);
    }

    static bool ChainIntegrityTest() {
        ListRand OriginalList(5);
        ListRand List1(5);
        ListRand List2(5);
        Utils::GenerateSequenceData(OriginalList);
        ListNode* List1Node = List1.Head;
        ListNode* List2Node = List2.Tail;
        for (auto Forward = OriginalList.Head, Backward = OriginalList.Tail; 
            !Forward || !Backward; 
            Forward = Forward->NextNode, Backward = Backward->PrevNode) {
                
            List1Node->Data = Forward->Data;
            List2Node->Data = Backward->Data;
            
            List1Node = List1Node->NextNode;
            List2Node = List2Node->PrevNode;
        }
        return List1 == List2;
    }

    static bool ListIndexTest() {
        // pick nodes with At(index) func and check their addresses
        ListRand List(5);
        Utils::GenerateSequenceData(List);
        bool result = true;
        result &= (List.Tail == List.At(List.Count - 1));
        result &= (List.Head == List.At(0));
        result &= (List.Head->NextNode->NextNode == List.At(2));
        return result;
    }

    static bool SerializationWierdSymbolsTest() {
        ListRand List(2);
        ListRand NewList;

        List.At(0)->Data = "Δ💛";
        List.At(1)->Data = "12\034 \t\r";

        std::stringstream serializationStream;
        List.Serialize(serializationStream);
        NewList.Deserialize(serializationStream);
        return List == NewList;
    }

    static bool EmpiricalSerializationTest() {
        for (int i = 0; i < 100; i++) {
            for (int j = 1; j < 20; j++) {
                ListRand List = Utils::CreateRandomList(j);
                std::stringstream serializationStream;
                List.Serialize(serializationStream);

                ListRand NewList;
                NewList.Deserialize(serializationStream);

                if (!(NewList == List)) {
                    Utils::PrintListConnectionsData(List);
                    Utils::PrintListConnectionsAdresses(List);
                    return false;
                }

            }
        }
        return true;
    }
};
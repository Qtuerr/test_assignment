#pragma once

#include <iostream>
#include <cassert>
#include <map>

#include "ListNode.h"

#define DELIMETER "\n"

class ListRand {
public:
    ListNode* Head = nullptr;
    ListNode* Tail = nullptr;
    int Count = 0;

    ListRand() : Count(0) {};
    ListRand(int count) : Count(count) {
        CreateList();
    };

    ListNode* At(int Index) {
        assert(Index >= 0 && Index < Count);
        ListNode* Node = Head;
        for (int i = 0; i < Index; i++) {
            Node = Node->NextNode;
        }
        return Node;
    }

    void Serialize(std::ostream& Stream) {
        std::map<ListNode*, int> NodeToID;
        NodeToID.emplace(nullptr, 0);
        int preventInfinitRecursion = 0; // just in case if chain integrity is violated
        for (ListNode* it = Head; it && preventInfinitRecursion < Count; it = it->NextNode) { // O(N)
            preventInfinitRecursion++;
            NodeToID.emplace(it, NodeToID.size());  // O(LogN)
        }


        Stream << Count << DELIMETER;

        preventInfinitRecursion = 0;
        for (ListNode* it = Head; it && preventInfinitRecursion < Count; it = it->NextNode) { // O(N)
            int& RandNodeID = NodeToID.at(it->RandomNode); // O(LogN)
            std::string Data = it->Data;
            Stream << RandNodeID << DELIMETER << Data.c_str() << DELIMETER;
        }
    }

    void Deserialize(std::istream& Stream) {
        Stream >> Count;
        CreateList();

        ListNode* Node = Head;
        for (int i = 0; i < Count; i++) { // O(N)
            int RandID;
            std::string Data;

            Stream >> RandID >> std::ws;
            std::getline(Stream, Data);

            Node->RandomNode = RandID ? At(RandID - 1) : nullptr; // O(N)
            Node->Data = Data;

            Node = Node->NextNode;
        }
    }

    bool operator== (ListRand& OtherList) {
        bool Result = true;
        bool IsLengthEqual = (OtherList.Count == Count);
        if (IsLengthEqual) {
            ListNode* OtherListIterator = OtherList.Head;
            ListNode* ThisListIterator = Head;
            for (int i = 0; i < this->Count; i++) {
                Result &= (*OtherListIterator == *ThisListIterator);
                OtherListIterator = OtherListIterator->NextNode;
                ThisListIterator = ThisListIterator->NextNode;
            }
        }
        return Result && IsLengthEqual;
    }

private:
    void CreateList() {
        assert(Count > 0);

        this->Head = new ListNode();
        ListNode* prevNode = this->Head;
        ListNode* nextNode = nullptr;
        for (int i = 1; i < Count; i++) {
            nextNode = new ListNode();
            nextNode->ChainAsNextNode(prevNode);
            prevNode = nextNode;
        }
        Tail = nextNode;
    }
};
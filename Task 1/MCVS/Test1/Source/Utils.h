#pragma once

#include "ListNode.h"

class Utils {
public:
    static ListRand CreateExampleList() {
        ListRand Result = ListRand(5);
        GenerateSequenceData(Result);
        Result.At(1)->RandomNode = Result.At(1);
        Result.At(3)->RandomNode = Result.At(0);
        Result.At(2)->RandomNode = Result.At(4);
        return Result;
    }

    static ListRand CreateRandomList(int count) {
        ListRand Result = ListRand(count);
        GenerateSequenceData(Result);
        GenerateRandomConnections(Result);
        return Result;
    }


    static void GenerateRandomConnections(ListRand& List) {
        for (ListNode* it = List.Head; it != nullptr; it = it->NextNode) {
            int randIndex = std::rand() % (List.Count + 1); // 0 stands for nullptr
            it->RandomNode = randIndex ? List.At(randIndex - 1) : nullptr;
        }

    }

    static void GenerateSequenceData(const ListRand& List, int offset = 0) {
        ListNode* Node = List.Head;
        for (int i = 0; i < List.Count; i++) {
            Node->Data = "data " + std::to_string(i + offset);
            Node = Node->NextNode;
        }
    }

    static void PrintListData(const ListRand& List) {
        ListNode* Node = List.Head;
        for (int i = 0; i < List.Count; i++) {
            std::cout << Node->Data << "\n";
            Node = Node->NextNode;
        }
    }

    static void PrintListConnectionsAdresses(const ListRand& List) {
        for (ListNode* it = List.Head; (it && it != it->NextNode); it = it->NextNode) {
            printf("Node Address: %p | NextNode: %p | PrevNode: %p | RandNode: %p \n",
                it, it->NextNode, it->PrevNode, it->RandomNode);
        }
    }

    static void PrintListConnectionsData(const ListRand& List) {
        for (ListNode* it = List.Head; (it && it != it->NextNode); it = it->NextNode) {
            printf("Node Data: %s | NextNode: %s | PrevNode: %s | RandNode: %s\n",
                it->Data.c_str(),
                (it->NextNode ? it->NextNode->Data.c_str() : "NULL"),
                (it->PrevNode ? it->PrevNode->Data.c_str() : "NULL"),
                (it->RandomNode ? it->RandomNode->Data.c_str() : "NULL"));
        }
    }
};
#pragma once

#include <string>

class ListNode {
public:
    ListNode* PrevNode = nullptr;
    ListNode* NextNode = nullptr;
    ListNode* RandomNode = nullptr;
    std::string Data;

    ListNode() {};

    void ChainAsNextNode(ListNode* PrevNode) {
        PrevNode->NextNode = this;
        this->PrevNode = PrevNode;
    }

    bool operator== (const ListNode& OtherNode) {
        bool result = true;
        result &= IsJointsEqual(PrevNode, OtherNode.PrevNode);
        result &= IsJointsEqual(NextNode, OtherNode.NextNode);
        result &= IsJointsEqual(RandomNode, OtherNode.RandomNode);
        result &= (OtherNode.Data == Data);
        return result;
    }

private:
    bool IsJointsEqual(ListNode* NodeJoint, ListNode* OtherNodeJoint) {
        bool bothNodesExists = NodeJoint && OtherNodeJoint;
        bool bothNodesNotExists = !(NodeJoint || OtherNodeJoint);
        bool dataEqual = false;
        if (bothNodesExists) {
            dataEqual = NodeJoint->Data == OtherNodeJoint->Data;
        }
        return dataEqual || bothNodesNotExists;
    }

};
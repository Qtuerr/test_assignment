#pragma once

#include <iostream>
#include <cassert>
#include <ctime>
#include <fstream> 
#include <map>
#include <sstream>

#include "Source/Tester.h"


#define DEBUG_PRINT false

int main() {
    std::srand(std::time(nullptr));

    assert(Tester::EqualityTest());
    assert(Tester::NonEqualityTest());
    assert(Tester::DifferentLengthTest());
    assert(Tester::ChainIntegrityTest());
    assert(Tester::ListIndexTest());
    assert(Tester::SerializationWierdSymbolsTest());
    assert(Tester::EmpiricalSerializationTest());


    ListRand List = Utils::CreateExampleList(); //Utils::CreateRandomList(10);

    std::stringstream serializationStream;
    List.Serialize(serializationStream);
    ListRand NewList;
    NewList.Deserialize(serializationStream);

    if (DEBUG_PRINT) {
        std::cout << "Original list nodes addresses\n";
        Utils::PrintListConnectionsAdresses(List);
        std::cout << "Serialization stream\n";
        std::cout << serializationStream.str();
        std::cout << "Deserialized list nodes addresses\n";
        Utils::PrintListConnectionsAdresses(NewList);
    }

    std::cout << "Original List\n";
    Utils::PrintListConnectionsData(List);
    std::cout << "Deserialized List\n";
    Utils::PrintListConnectionsData(NewList);

    std::cout << (List == NewList? "SUCCESS": "FAILURE");

    return 0;
}
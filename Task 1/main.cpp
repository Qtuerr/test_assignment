#include <iostream>
#include <cassert>
#include <ctime>
#include <fstream> 
#include <map>
#include <sstream>

using namespace std;

#define DELIMETER "\n"
#define DEBUG_PRINT true

class ListNode {
public:
    ListNode* PrevNode = nullptr;
    ListNode* NextNode = nullptr;
    ListNode* RandomNode = nullptr;
    std::string Data;
    
    ListNode() {};
        
    void ChainAsNextNode(ListNode* PrevNode) {
        PrevNode->NextNode = this;
        this->PrevNode = PrevNode;
    }
    
    bool operator== (const ListNode& OtherNode) {
        bool result = true;
        result &= IsJointsEqual(PrevNode, OtherNode.PrevNode);
        result &= IsJointsEqual(NextNode, OtherNode.NextNode);
        result &= IsJointsEqual(RandomNode, OtherNode.RandomNode);
        result &= (OtherNode.Data == Data);
        return result;
    }

private:
    bool IsJointsEqual(ListNode* NodeJoint, ListNode* OtherNodeJoint) {
        bool bothNodesExists = NodeJoint && OtherNodeJoint;
        bool bothNodesNotExists = !(NodeJoint || OtherNodeJoint);
        bool dataEqual = false;
        if (bothNodesExists) {
            dataEqual = NodeJoint->Data == OtherNodeJoint->Data;
        }
        return dataEqual || bothNodesNotExists;
    }

};

class ListRand {
public:
    ListNode* Head = nullptr;
    ListNode* Tail = nullptr;
    int Count = 0;

    ListRand() : Count(0) {};
    ListRand(int count) : Count(count) {
        CreateList();
    };

    ListNode* At(int Index) {
        assert(Index >= 0 && Index < Count);
        ListNode* Node = Head;
        for (int i = 0; i < Index; i++) {
            Node = Node->NextNode;
        }
        return Node;
    }

    void Serialize(std::ostream& Stream) {
        std::map<ListNode*, int> NodeToID;
        NodeToID.emplace(nullptr, 0);
        int preventInfinitRecursion = 0; // just in case if chain integrity is violated
        for (ListNode* it = Head; it && preventInfinitRecursion < Count; it = it->NextNode) { // O(N)
            preventInfinitRecursion++;
            NodeToID.emplace(it, NodeToID.size());  // O(LogN)
        }


        Stream << Count << DELIMETER;

        preventInfinitRecursion = 0;
        for (ListNode* it = Head; it && preventInfinitRecursion < Count; it = it->NextNode) { // O(N)
            int& RandNodeID = NodeToID.at(it->RandomNode); // O(LogN)
            std::string Data = it->Data;
            Stream << RandNodeID << DELIMETER << Data.c_str() << DELIMETER;
        }
    }

    void Deserialize(std::istream& Stream) {
        Stream >> Count;
        CreateList();

        ListNode* Node = Head;
        for (int i = 0; i < Count; i++) { // O(N)
            int RandID;
            std::string Data;

            Stream >> RandID >> std::ws;
            std::getline(Stream, Data);

            Node->RandomNode = RandID ? At(RandID - 1) : nullptr; // O(N)
            Node->Data = Data;

            Node = Node->NextNode;
        }
    }

    bool operator== (ListRand& OtherList) {
        bool Result = true;
        bool IsLengthEqual = (OtherList.Count == Count);
        if (IsLengthEqual) {
            ListNode* OtherListIterator = OtherList.Head;
            ListNode* ThisListIterator = Head;
            for (int i = 0; i < this->Count; i++) {
                Result &= (*OtherListIterator == *ThisListIterator);
                OtherListIterator = OtherListIterator->NextNode;
                ThisListIterator = ThisListIterator->NextNode;
            }
        }
        return Result && IsLengthEqual;
    }

private:
    void CreateList() {
        assert(Count > 0);

        this->Head = new ListNode();
        ListNode* prevNode = this->Head;
        ListNode* nextNode = nullptr;
        for (int i = 1; i < Count; i++) {
            nextNode = new ListNode();
            nextNode->ChainAsNextNode(prevNode);
            prevNode = nextNode;
        }
        Tail = nextNode;
    }
};

class Utils {
public:
    static ListRand CreateExampleList() {
        ListRand Result = ListRand(5);
        GenerateSequenceData(Result);
        Result.At(1)->RandomNode = Result.At(1);
        Result.At(3)->RandomNode = Result.At(0);
        Result.At(2)->RandomNode = Result.At(4);
        return Result;
    }

    static ListRand CreateRandomList(int count) {
        ListRand Result = ListRand(count);
        GenerateSequenceData(Result);
        GenerateRandomConnections(Result);
        return Result;
    }


    static void GenerateRandomConnections(ListRand& List) {
        for (ListNode* it = List.Head; it != nullptr; it = it->NextNode) {
            int randIndex = std::rand() % (List.Count + 1); // 0 stands for nullptr
            it->RandomNode = randIndex ? List.At(randIndex - 1) : nullptr;
        }

    }

    static void GenerateSequenceData(const ListRand& List, int offset = 0) {
        ListNode* Node = List.Head;
        for (int i = 0; i < List.Count; i++) {
            Node->Data = "data " + std::to_string(i + offset);
            Node = Node->NextNode;
        }
    }

    static void PrintListData(const ListRand& List) {
        ListNode* Node = List.Head;
        for (int i = 0; i < List.Count; i++) {
            std::cout << Node->Data << "\n";
            Node = Node->NextNode;
        }
    }

    static void PrintListConnectionsAdresses(const ListRand& List) {
        for (ListNode* it = List.Head; (it && it != it->NextNode); it = it->NextNode) {
            printf("Node Address: %p | NextNode: %p | PrevNode: %p | RandNode: %p \n",
                it, it->NextNode, it->PrevNode, it->RandomNode);
        }
    }

    static void PrintListConnectionsData(const ListRand& List) {
        for (ListNode* it = List.Head; (it && it != it->NextNode); it = it->NextNode) {
            printf("Node Data: %s | NextNode: %s | PrevNode: %s | RandNode: %s\n",
                it->Data.c_str(),
                (it->NextNode ? it->NextNode->Data.c_str() : "NULL"),
                (it->PrevNode ? it->PrevNode->Data.c_str() : "NULL"),
                (it->RandomNode ? it->RandomNode->Data.c_str() : "NULL"));
        }
    }
};

class Tester {
public:
    static bool EqualityTest() {
        ListRand List1(5);
        ListRand List2(5);
        Utils::GenerateSequenceData(List1);
        Utils::GenerateSequenceData(List2);
        return List1 == List2;
    }
    
    static bool NonEqualityTest() {
        ListRand List1(5);
        ListRand List2(5);
        Utils::GenerateSequenceData(List1);
        Utils::GenerateSequenceData(List2, 2);
        return !(List1 == List2);
    }
    
    static bool DifferentLengthTest() {
        ListRand List1(5);
        ListRand List2(2);
        Utils::GenerateSequenceData(List1);
        Utils::GenerateSequenceData(List2);
        return !(List1 == List2);
    }
    
    static bool ChainIntegrityTest() {
        ListRand OriginalList(5);
        ListRand List1(5);
        ListRand List2(5);
        Utils::GenerateSequenceData(OriginalList);
        ListNode* List1Node = List1.Head;
        ListNode* List2Node = List2.Tail;
        for (auto Forward = OriginalList.Head, Backward = OriginalList.Tail; 
            !Forward || !Backward; 
            Forward = Forward->NextNode, Backward = Backward->PrevNode) {
                
            List1Node->Data = Forward->Data;
            List2Node->Data = Backward->Data;
            
            List1Node = List1Node->NextNode;
            List2Node = List2Node->PrevNode;
        }
        return List1 == List2;
    }
    
    static bool ListIndexTest() {
        // pick nodes with At(index) func and check their addresses
        ListRand List(5);
        Utils::GenerateSequenceData(List);
        bool result = true;
        result &= (List.Tail == List.At(List.Count-1)); 
        result &= (List.Head == List.At(0)); 
        result &= (List.Head->NextNode->NextNode == List.At(2)); 
        return result;
    }
    
    static bool SerializationWierdSymbolsTest() {
        ListRand List(2);
        ListRand NewList;
        
        List.At(0)->Data = "Δ💛";
        List.At(1)->Data = "12\034 \t\r";
        
        std::stringstream serializationStream;
        List.Serialize(serializationStream);
        NewList.Deserialize(serializationStream);
        return List == NewList;
    }
    
    static bool EmpiricalSerializationTest() {
        for (int i = 0; i < 100; i++) {
            for (int j = 1; j < 20; j++) {
                ListRand List = Utils::CreateRandomList(j);
                std::stringstream serializationStream;
                List.Serialize(serializationStream);
                
                ListRand NewList;
                NewList.Deserialize(serializationStream);
                
                if (!(NewList == List)) {
                    Utils::PrintListConnectionsData(List);
                    Utils::PrintListConnectionsAdresses(List);
                    return false;
                }
                
            }
        }
        return true;
    }
};


int main()
{
std::srand(std::time(nullptr));

    assert(Tester::EqualityTest());
    assert(Tester::NonEqualityTest());
    assert(Tester::DifferentLengthTest());
    assert(Tester::ChainIntegrityTest());
    assert(Tester::ListIndexTest());
    assert(Tester::SerializationWierdSymbolsTest());
    assert(Tester::EmpiricalSerializationTest());


    ListRand List = Utils::CreateRandomList(10);
    std::stringstream serializationStream;
    List.Serialize(serializationStream);
    ListRand NewList;
    NewList.Deserialize(serializationStream);

    if (DEBUG_PRINT) {
        std::cout << "Original list nodes addresses\n";
        Utils::PrintListConnectionsAdresses(List);
        std::cout << "Serialization stream\n";
        std::cout << serializationStream.str();
        std::cout << "Deserialized list nodes addresses\n";
        Utils::PrintListConnectionsAdresses(NewList);
    }

    std::cout << "Original List\n";
    Utils::PrintListConnectionsData(List);
    std::cout << "Deserialized List\n";
    Utils::PrintListConnectionsData(NewList);

    std::cout << (List == NewList? "SUCCESS": "FAILURE");

    return 0;
}
